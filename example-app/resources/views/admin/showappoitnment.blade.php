<!DOCTYPE html>
<html lang="en">
  <head>
    @include('admin.css')
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_sidebar.html -->
      @include('admin.sidebar')
      <!-- partial -->
      @include('admin.navbar')
        
      <div class="container-fluid page-body-wrapper">

      <div align="center"  style="padding-top:100px;">
      <table>
          <tr style="background-color: black; color:white;">
              <th style="padding: 10px">Nama Pasien</th>
              <th style="padding: 10px">Email</th>
              <th style="padding: 10px">Handphone</th>
              <th style="padding: 10px">Nama Dokter</th>
              <th style="padding: 10px">Tanggal</th>
              <th style="padding: 10px">Keluhan</th>
              <th style="padding: 10px">Status</th>
              <th style="padding: 10px">Approved</th>
              <th style="padding: 10px">Cancel</th>
              
          </tr>
          @foreach($data as $appoint)
          <tr style="background-color: black;">

            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->name}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->email}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->phone}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->doctor}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->date}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->message}}</td>
            <td style="padding:10px; font-size 20px; color:white;">{{$appoint->status}}</td>
            <td> 
                <a class ="btn btn-success"href="{{url('approved', $appoint->id)}}">Approved</a>
            </td>
            <td> 
                <a class ="btn btn-danger" href="{{url('canceled',$appoint->id)}}">Canceled</a>
            </td>

      </tr>
      @endforeach
      </table>
    </div>
    </div>

      </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    @include('admin.script')
    <!-- End custom js for this page -->
  </body>
</html>